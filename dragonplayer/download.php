<?php
  $page_title = 'Downloading Dragon Player';
  $site_root = "./";
  include "multimedia.inc";
  include 'header.inc';
?>

<p><h2>Requirements</h2><br />
You will need at least:
<ul>
  <li>kdelibs 4.0
  <li>kdebase 4.0, with video/XCB support
  <li>xine-lib 1.1, built with XCB support
</ul>
CMake is also required to build Dragon Player.</p>

<p><h2>Last Release Download</h2>
<ul>
  <li><a href="downloaded/dragonplayer-2.0.1.tar.bz2">Dragon Player 2.0.1</a>
</ul>
</p>

<p><h2>Legacy</h2>
<ul>
  <li><a href="downloaded/dragonplayer-2.0.tar.bz2">Dragon Player 2.0  First stable release</a>
  <li><a href="downloaded/dragonplayer-2.0_rc1.tar.bz2">Dragon Player 2.0 Release Candidate</a>
  <li><a href="downloaded/dragonplayer-2.0_beta1.tar.bz2">Dragon Player 2.0 beta1</a>
  <li><a href="downloaded/dragonplayer-2.0_alpha1.tar.bz2">Dragon Player 2.0 alpha1</a>
</ul>
</p>

<p><h2>Kubuntu Packages</h2><br />
If you run Kubuntu Gutsy 7.10, installing Dragon Player is easy. Enable <a href="https://help.ubuntu.com/community/UbuntuBackports#head-180fe9eb86a3434e066db173a2d276a8c003b913"> the backports repo </a> and then install Dragon Player using Adept Manager or the command `apt-get install dragonplayer`.</p>

<p><h2>Suse Packages</h2>
<ul>
  <li><a href="http://software.opensuse.org/search?baseproject=openSUSE%3A10.3&p=1&q=dragonplayer">10.3</a>
  <li><a href="http://software.opensuse.org/search?baseproject=openSUSE%3AFactory&p=1&q=dragonplayer">Factory</a>
</ul>

<p><h2>Building the SVN Version</h2><br />
The SVN version is the latest development version and it may at any time be broken. You can download and build it with the following commands:
<ul>
  <li>svn co svn://anonsvn.kde.org/home/kde/trunk/KDE/kdemultimedia
  <li>cd kdemultimedia
  <li>mkdir build
  <li>cd build
  <li>cmake ..
  <li>cd dragonplayer
  <li>make install
</ul>
</p>

<?php
INCLUDE 'footer.inc';
?>
 
