<link rel="stylesheet" type="text/css" href="main.css" />

<?php
  $site_root = "./";
  $site_menus = 1;
  include "multimedia.inc";
  include 'header.inc';

  function addProgram( $description, $name, $link, $pic, $bool_link )
  {
    echo "<div class='programbox'>\n";
    if ($bool_link)
     {
      echo "<div align='center'><a href='$link'>\n";
      echo "<img src='$pic' width='32' height='32' border='0' hspace='10' vspace='10' alt='$name' title='$description' />\n";
      echo "</a></div>\n";
      echo "<div class='programbox-text'><a href='$link'>$name</a></div>\n";
     }
     else
     {
      echo "<div align='center'>\n";
      echo "<img src='$pic' width='32' height='32' border='0' hspace='10' vspace='10' alt='$name' title='$description' />\n";
      echo "</div>\n";
      echo "<div class='programbox-text'>$name</div>\n";
     }
    echo "</div>\n";
  }

?>

<h1>The KDE Multimedia Project</h1>

<p>KDE Multimedia development aims to provide both users and developers consistent, flexible, and useful interfaces. Applications, KParts, libraries, and plugins all further this goal. In KDE 2 and 3 these interfaces are largely built on the daemon and libraries of the Analog Real-Time Synthesizer, known as aRts. The aRts back-end provides KDE with a portable sound server for playback and recording, a shared library of media decoding, and a framework for realtime sound processing</p>

<p>With KDE 4 aRts was discontinued and Phonon, a new API, was used by the KDE4 programs. Phonon is a library situated between the KDE programs and the media backends. It's objective is allow the programmer to only have to write one piece of code to work with all the Phonon backends.</p>

<h2><a name="programs">The Line Up of KDE Multimedia <?php echo $kde_current_family; ?></a></h2>
<?php
  addProgram("Audio Player", "Juk", "http://developer.kde.org/~wheeler/juk.html", "images/projects/32-app-juk.png", 1);
  addProgram("Digital Mixer", "KMix", "kmix", "images/projects/32-app-kmix.png", 0);
  addProgram("Audio CD Player", "KsCD", "kscd", "images/projects/32-app-kscd.png", 0);
  addProgram("Video Player", "Dragon Player", "dragon", "images/projects/32-app-dragonplayer.png", 0);
?>

<div style="clear: both;"></div>
<h3><a name="programs">Headliners</a></h3>
<?php
  addProgram("Audio Player", "Amarok", "http://amarok.kde.org", "images/projects/32-app-amarok.png", 1);
  addProgram("Burning Program", "K3b", "http://k3b.plainblack.com", "images/projects/32-app-k3b.png", 1);
  addProgram("Audio Creator", "KaudioCreator", "http://www.icefox.net/programs/?program=KAudioCreator", "images/projects/32-app-kaudiocreator.png", 1);
  addProgram("Media Player", "KMPlayer", "http://kmplayer.kde.org", "images/projects/32-app-kmplayer.png", 1);
  addProgram("Audio Player", "KPlayer", "http://kplayer.sourceforge.net/", "images/projects/32-app-kplayer.png", 1);
  addProgram("Video Player", "Kaffeine", "http://kaffeine.sourceforge.net", "images/projects/32-app-kaffeine.png", 1);

?>

<div style="clear: both;"></div>
<h2><a name="programs">Libraries in KDE Multimedia <?php echo $kde_current_family; ?></a></h2>
<?php
  addProgram("Phonon", "Phonon", "http://phonon.kde.org", "images/projects/32-app-phonon.png", 1);

?>
<?php
INCLUDE 'footer.inc';
?>
