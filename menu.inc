<?php
$this->setName ("KDE Multimedia Homepage");

$section =& $this->appendSection("Navigation");
$section->appendLink("Home", "");
$section->appendLink("KDE Home", "http://www.kde.org", false);

$section =& $this->appendSection("Software");
$section->appendLink("KDE Documentation", "http://docs.kde.org/stable/en/kdemultimedia/", false);

$section =& $this->appendSection("Development");
$section->appendLink("Phonon Documentation", "phonon.php", false);
$section->appendLink("KDE4 API", "http://api.kde.org/", false);

$section =& $this->appendSection("Contact");
$section->appendLink("Contact", "contact.php");
$section->appendLink("Mailing Lists", "http://www.kde.org/mailinglists/", false);
?>
