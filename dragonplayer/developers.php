<?php
  $page_title = 'Developers';
  $site_root = "./";
  include "multimedia.inc";
  include 'header.inc';
?>

<table border=1>
  <tr>
    <td><b>Name</b></td>
    <td><b>IRC Nickname</b></td>
    <td><b>SVN Nickname</b></td>
    <td><b>Email</b></td>
  </tr>
  <tr>
    <td>Ian Monroe</td>
    <td>eean</td>
    <td>ianmonroe</td>
    <td><a href="mailto:ianmonroe@dragonplayer.org">ianmonroe@dragonplayer.org</a></td>
  </tr>
  <tr>
    <td>Davide Emudson</td>
    <td>davide_emudson</td>
    <td>davidedmundson</td>
    <td><a href="mailto:kde@davidedmundson.co.uk">kde@davidedmundson.co.uk</a></td>
  </tr>
  <tr>
    <td>Fabio Locati</td>
    <td>fale</td>
    <td>flocati</td>
    <td><a href="mailto:fabiolocati@gmail.com">fabiolocati@gmail.com</a></td>
  </tr>
</table>

<?php
INCLUDE 'footer.inc';
?>
 
