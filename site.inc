<?php
include_once ("functions.inc");
$templatepath = "newlayout/";
$site_title = "KDE Multimedia";
$site_external = true;
$name = "multimedia.kde.org Webmaster";
$mail = "fabiolocati@gmail.com";
$showedit = false;

$kde_current_version = "4.0";
$kde_coming_version = "4.1";
$kde_current_family = "4";
$kde_stable_doc_path = "http://docs.kde.org/stable/en/kdeedu/";
$kde_development_doc_path = "http://docs.kde.org/development/en/kdeedu/";
?>