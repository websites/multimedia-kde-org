<?php
  $page_title = 'Phonon Documentation';
  $site_root = "./";
  include "multimedia.inc";
  include 'header.inc';
?>
<p>Documentation for the different versions of Phonon can be found at:<br />
<ul>
<li><a href="http://api.kde.org/4.0-api/kdelibs-apidocs/phonon/html/index.html"><b>KDE 4.0 docs</b></a><br /><br />
<li><a href="http://doc.trolltech.com/4.4beta/phonon.html"><b>Qt 4.4 docs (libphonon 4.1)</b></a><br /><br />
<li><a href="http://api.kde.org/4.x-api/kdelibs-apidocs/phonon/html/index.html"><b>KDE 4.1 docs (libphonon 4.2)</b></a><br /><br />
</ul>
</p>
<?php
INCLUDE 'footer.inc';
?>