<link rel="stylesheet" type="text/css" href="main.css" />

<?php
  $page_title = 'Dragon Player';
  $site_root = "./";
  $site_menus = 1;
  include "multimedia.inc";
  include 'header.inc';
?>

<img style="float:right" src="images/dragon_128.png">

<p>Dragon Player is a simple video player for KDE 4 developed by Ian Monroe. The KDE 3 version was developed by Max Howell and known as Codeine.</p>

<br /><br /><br /><br /><br />
<p><h2>Features</h2></p>
<ul>
  <li>Simple interface that does not get in the way
  <li>Resumes where you left off when replaying a video
  <li>Support for subtitles
  <li>Video display settings (brightness, contrast)
</ul>
<img width=540 src="images/ss1.png">
<?php
INCLUDE 'footer.inc';
?> 
