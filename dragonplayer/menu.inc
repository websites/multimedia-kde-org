<?php
$this->setName ("Dragon Player Homepage");

$section =& $this->appendSection("Navigation");
$section->appendLink("Home", "");
$section->appendLink("KDE Multimedia Home", "http://multimedia.kde.org", false);
$section->appendLink("KDE Home", "http://www.kde.org", false);

$section =& $this->appendSection("Dragon Player");
//$section->appendLink("Screenshots", "screenshots.php");
$section->appendLink("Download", "download.php");
$section->appendLink("Install", "installing.php");
$section->appendLink("Contact", "contact.php");
?>
