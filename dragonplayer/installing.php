<?php
  $page_title = 'Installing Dragon Player on your system';
  $site_root = "./";
  include "multimedia.inc";
  include 'header.inc';
?>

<p><h2>Requirements</h2><br />
You will need at least:
<ul>
  <li>kdelibs 4.0
  <li>kdebase 4.0, with video/XCB support
  <li>xine-lib 1.1, built with XCB support
  <li>CMake
</ul>

<p><h2>Kubuntu</h2><br />
If you run Kubuntu Gutsy 7.10, installing Dragon Player is easy. Enable <a href="https://help.ubuntu.com/community/UbuntuBackports#head-180fe9eb86a3434e066db173a2d276a8c003b913"> the backports repo </a> and then install Dragon Player using Adept Manager or the command `apt-get install dragonplayer`.</p>

<p><h2>Suse</h2>
<ul>
  <li><a href="http://software.opensuse.org/search?baseproject=openSUSE%3A10.3&p=1&q=dragonplayer">10.3</a>
  <li><a href="http://software.opensuse.org/search?baseproject=openSUSE%3AFactory&p=1&q=dragonplayer">Factory</a>
</ul>

<p><h2>SVN (last commits)</h2><br />
The SVN version is the latest development version and it may at any time be broken. You can download and build it with the following commands:
<ul>
  <li>svn co svn://anonsvn.kde.org/home/kde/trunk/KDE/kdemultimedia
  <li>cd kdemultimedia
  <li>mkdir build
  <li>cd build
  <li>cmake ..
  <li>cd dragonplayer
  <li>make install
</ul>
</p>

<?php
INCLUDE 'footer.inc';
?>
 
 
