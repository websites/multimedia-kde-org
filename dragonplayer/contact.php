<?php
  $page_title = 'Contacting Developers';
  $site_root = "./";
  include "multimedia.inc";
  include 'header.inc';
?>

<p><b>Bugs and Features</b><br />
Problem reports and Feature requests about particular KDE applications and libraries should be directed to the <a href="http://bugs.kde.org/">KDE Bug Tracking System</a>. Dragon player have <a href="https://bugs.kde.org/buglist.cgi?product=dragonplayer"> this </a> section of bugzilla.</p>

<p><b>IRC</b><br />
On the Freenode network you can reach some of us on the <a href="irc://irc.freenode.net/dragonplayer">#dragonplayer</a> channel.</p>

<p><b>E-Mail</b><br />
<a href="mailto:dragon-bugs@dragonplayer.org">dragon-bugs@dragonplayer.org</a>
</p>

<?php
INCLUDE 'footer.inc';
?>
