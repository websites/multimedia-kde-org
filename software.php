<?php
  $page_title = 'KDE Multimedia Software Library';
  $site_root = "./";
  include "multimedia.inc";
  include 'header.inc';
?>

<p>Following is a partial list of multimedia applications, services, and
plugins available for KDE.  Most are shipped with KDE, but some are available
separately.</p>

<ul>
	<li><a href="http://amarok.kde.org">Amarok</a> - Music Player</li>
	<li><a href="http://brahms.sourceforge.net/">Brahms</a> - Sequencer/Notation Editor</li>
	<li><a href="http://developer.kde.org/~wheeler/juk.html">Juk</a> - Music Jukebox</li>
	<li><a href="http://www.k3b.org/">K3b</a> - CD/DVD-Burning</li>
	<li><a href="http://kaffeine.sourceforge.net/">Kaffeine</a> - Media Player (xine, GStreamer)</li>
	<li><a href="http://kde-apps.org/content/show.php?content=107645">KAudioCreator</a> - CD Ripper</li>
	<li><a href="http://docs.kde.org/stable/en/kdemultimedia/kmix/index.html">KMix</a> - Sound Mixer</li>
	<li><a href="http://kmplayer.kde.org">KMPlayer</a> - Media Player (MPlayer, xine)</li>
	<li><a href="http://kplayer.sourceforge.net">KPlayer</a> - Media Player (MPlayer)</li>
	<li><a href="http://kradio.sourceforge.net">KRadio</a> - Radio Application</li>
	<li><a href="http://docs.kde.org/stable/en/kdemultimedia/kscd/index.html">KSCD</a> - CD Player</li>
	<li><a href="http://www.rosegardenmusic.com/">Rosegarden</a> - Sequencer/Notation Editor</li>
</ul>

For more applications visit <a href="http://www.kde-apps.org/index.php?xcontentmode=220x221">kde-apps.org</a>.

<?php
INCLUDE 'footer.inc';
?>
