<?php
  $page_title = 'Contacting Developers';
  $site_root = "./";
  include "multimedia.inc";
  include 'header.inc';
?>

<p><b>FAQ</b><br />
To get answers to questions about using KDE Multimedia applications see the <a href="http://www.kde.org/info/faq.php">KDE Getting Answers to Questions</a> page.</p>

<p><b>Bugs and Features</b><br />
Problem reports and Feature requests about particular KDE applications and libraries should be directed to the <a href="http://bugs.kde.org/">KDE Bug Tracking System</a>.</p>

<p><b>IRC</b><br />
On the Freenode network you can reach some of us on the <a href="irc://irc.freenode.net/kde-multimedia">#kde-multimedia</a> channel.</p>

<p><b>Mailing-List</b><br />
Questions and discussion about KDE Multimedia development should be directed to the <a href="https://mail.kde.org/mailman/listinfo/kde-multimedia">kde-multimedia</a> mailing list. Here you can find the <a href="http://lists.kde.org/?l=kde-multimedia&r=1&w=2">Archives</a></p>

<?php
INCLUDE 'footer.inc';
?>
